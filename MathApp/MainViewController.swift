//
//  MainViewController.swift
//  MathApp
//
//  Created by Evert van der Weit on 22/02/16.
//  Copyright © 2016 Evert van der Weit. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    override func viewDidLoad() {
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        print("ACT-R level \(OptionValues.sharedInstance.actrLevel)")
        print("Game number \(OptionValues.sharedInstance.gameNumber)")
        print("Game difficulty \(OptionValues.sharedInstance.gameDifficulty)")
        print("Auto adjust \(OptionValues.sharedInstance.autoAdjust)")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func QuitButton(sender: UIButton) {
        if sender.currentTitle! == "Quit" {
            exit(0)
        }
    }
}

// A shared class to keep track of the optional settings
class OptionValues {
    static let sharedInstance = OptionValues()
    var gameNumber = 10
    var gameDifficulty = 20
    var actrLevel = 3
    var autoAdjust = 1
    var difficultyLabel = "Easy"
    var levelLabel = "Beginner"
    var modelDiffFactor = 2.25
    var running = 0
    
    // Init the brain at start of app
    var actrBrain:MathBrain
    init () {
        actrBrain = MathBrain(maxGoal: gameDifficulty)
    }
}
