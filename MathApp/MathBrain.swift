//
//  MathBrain.swift
//  MathApp
//
//  Created by Henk Composttank on 3/3/16.
//  Copyright © 2016 Evert van der Weit. All rights reserved.
//

import Foundation

class MathBrain {
    var actrModel = Model()
    let formatter = NSNumberFormatter()
    
    func getMaxValue(val1: Int, val2: Int) -> (Double, Double) {
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        print("Finding max value, value1: \(val1), value2: \(val2)")
        let timeStart = actrModel.time
        // Create a chunk to compare the values
        let goalChunk = Chunk(s: "goal", m: actrModel)
        goalChunk.setSlot("isa", value: "maxnumber")
        goalChunk.setSlot("ten1", value: Double(val1 / 10))
        goalChunk.setSlot("one1", value: Double(val1 % 10))
        goalChunk.setSlot("ten2", value: Double(val2 / 10))
        goalChunk.setSlot("one2", value: Double(val2 % 10))
        goalChunk.setSlot("state", value: "start")
      
        actrModel.buffers = [:]
        actrModel.buffers["goal"] = goalChunk
        
        // First get the answer for the singles
        actrModel.run()
        var answer = (formatter.numberFromString(
            actrModel.lastAction("one-ans")!
            )?.doubleValue)!
        
        // Then for the tens
        actrModel.run()
        
        answer += ((formatter.numberFromString(
            actrModel.lastAction("ten-ans")!
            )?.doubleValue)!*10)
        print("The result was \(answer)")
        return(actrModel.time-timeStart, answer)
    }
    
    func addition(val1: Int, val2: Int) -> (Double, Double) {
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        print("Adding \(val1) to \(val2)")

        // Create the addition goal chunk
        let timeStart = actrModel.time
        let goalChunk = Chunk(s: "goal", m: actrModel)
        goalChunk.setSlot("isa",  value: "addition")
        goalChunk.setSlot("ten1", value: Double(val1 / 10))
        goalChunk.setSlot("one1", value: Double(val1 % 10))
        goalChunk.setSlot("ten2", value: Double(val2 / 10))
        goalChunk.setSlot("one2", value: Double(val2 % 10))
        goalChunk.setSlot("state", value: "start")

        actrModel.buffers = [:]
        actrModel.buffers["goal"] = goalChunk
        
        // First get the answer for the singles
         actrModel.run()
         var answer = (formatter.numberFromString(
            actrModel.lastAction("one-ans")!
            )?.doubleValue)!

        // Then for the tens
        actrModel.run()
        
        answer += ((formatter.numberFromString(
            actrModel.lastAction("ten-ans")!
            )?.doubleValue)!*10)
        print("The result was \(answer)")
        return(actrModel.time-timeStart, answer)
    }
    
    func subtract(val1: Int, val2: Int) -> (Double, Double) {
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        print("Subtracting \(val2) from \(val1)")
        // Subtract val2 from val1
        let timeStart = actrModel.time
        let goalChunk = Chunk(s: "goal", m: actrModel)
        goalChunk.setSlot("isa",  value: "subtraction")
        goalChunk.setSlot("ten1", value: Double(val1/10))
        goalChunk.setSlot("one1", value: Double(val1%10))
        goalChunk.setSlot("ten2", value: Double(val2/10))
        goalChunk.setSlot("one2", value: Double(val2%10))
        goalChunk.setSlot("state", value: "start")
        
        actrModel.buffers = [:]
        actrModel.buffers["goal"] = goalChunk
        actrModel.run()
        
        let answer = formatter.numberFromString(actrModel.lastAction("ans")!)?.doubleValue
        print("The result was \(answer)")
        return(actrModel.time-timeStart, answer!)
    }
    
    init(maxGoal: Int) {
        // Load the model (production rules)
        actrModel.loadModel("ModelMathApp")
       
        // Generate the chunks
        // The numbers chunks
        //  Used for addition
        for index in 0...10 {
            for index2 in 0...9 {
                let diff = index+index2
                let name = "num\(index)+\(index2)"
                let tempChunk = Chunk(s: name, m: actrModel)
                tempChunk.setSlot("isa", value: "numbers")
                tempChunk.setSlot("nr1", value: Double(index))
                tempChunk.setSlot("nr2", value: Double(index2))
                tempChunk.setSlot("result", value: Double(diff))
                tempChunk.fixedActivation = 1.0
                actrModel.dm.addToDM(tempChunk)
            }
        }
        
        // The difference chunks
        //  Used for subtraction and determining the max value
        for index in 0...9 {
            for index2 in 0...9 {
                let diff = index-index2
                let name = "diff\(index)-\(index2)"
                let tempChunk = Chunk(s: name, m: actrModel)
                tempChunk.setSlot("isa", value: "difference")
                tempChunk.setSlot("nr1", value: Double(index))
                tempChunk.setSlot("nr2", value: Double(index2))
                tempChunk.setSlot("result", value: Double(diff))
                tempChunk.fixedActivation = 1.0
                actrModel.dm.addToDM(tempChunk)
            }
        }
        // The multiply chunks
        //  Used for subtraction
        for index2 in 0...10 {
            let index = 10
            let diff = index*index2
            let name = "mult\(index)*\(index2)"
            let tempChunk = Chunk(s: name, m: actrModel)
            tempChunk.setSlot("isa", value: "multiply")
            tempChunk.setSlot("nr1", value: Double(index))
            tempChunk.setSlot("nr2", value: Double(index2))
            tempChunk.setSlot("result", value: Double(diff))
            tempChunk.fixedActivation = 1.0
            actrModel.dm.addToDM(tempChunk)
        }
        // The difference-answer chunks
        //  Used for subtraction
        for index in 0.stride(through: 90, by: 10) {
            for index2 in -9...9 {
                let diff = index+index2
                let name = "diff-ans\(index)+\(index2)"
                let tempChunk = Chunk(s: name, m: actrModel)
                tempChunk.setSlot("isa", value: "difference-ans")
                tempChunk.setSlot("nr1", value: Double(index))
                tempChunk.setSlot("nr2", value: Double(index2))
                tempChunk.setSlot("result", value: Double(diff))
                tempChunk.fixedActivation = 1.0
                actrModel.dm.addToDM(tempChunk)
            }
        }
    }
}