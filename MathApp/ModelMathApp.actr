
;;-----------------------------------FIND_MAX_VALUE------------------------------------------------------------

;;chunk needed: (isa goal ten1 one1 ten2 one2 ten-ans one-ans state)
;;chunk (isa difference nr1 nr2 result)


;;if there are two “ten” numbers selected, start retrieving them both
(p retrieve-max-values-tens
    =goal>
     isa maxnumber
     state start
     ten1 =num1
     ten2 =num2
    ==>
    =goal>
     isa maxnumber
     state decide
    +retrieval>
     isa difference
     nr1 =num1
     nr2 =num2
)

;;if the result of “tens” nr1-nr2 == 0, the “tens” do not determine the highest value and the “ones” will determine the maximum value
(p tens-values-are-equal
    =goal>
     isa maxnumber
     state decide
     ten1 =num1
     one1 =num2
     ten2 =num3
     one2 =num4
    =retrieval>
     isa difference
     nr1 =num1
     nr2 =num3
     result =result
     result 0
    ==>
    =goal>
     isa maxnumber
     state decide-ones
    +retrieval>
     isa difference
     nr1 =num2
     nr2 =num4
)

;if the result of the “ones” nr1-nr2 is a positive value, value1 is the maximum value
(p ones-value1-is-max
    =goal>
     isa maxnumber
     state decide-ones
     ten1 =num1
     one1 =num2
     ten2 =num3
     one2 =num4
    =retrieval>
     isa difference
     nr1 =num2
     nr2 =num4
     result =result
    < result 1
    ==>
    =goal>
     isa maxnumber
     state done
     ten-ans =num1
     one-ans =num2
    +action>
     isa outcome
     one-ans =num2
     ten-ans =num1
)

;if the result of the “ones” nr1-nr2 is a negative value, value2 is the maximum value
(p ones-value2-is-max
    =goal>
     isa maxnumber
     state decide-ones
     ten1 =num1
     one1 =num2
     ten2 =num3
     one2 =num4
    =retrieval>
     isa difference
     nr1 =num2
     nr2 =num4
     result =result
    > result -1
    ==>
    =goal>
     isa maxnumber
     state done
     ten-ans =num3
     one-ans =num4
    +action>
     isa outcome
     one-ans =num4
     ten-ans =num3
)


;;if the result of “tens” nr1-nr2 is a positive value, number1 is the maximum value
(p tens-value1-is-max
    =goal>
     isa maxnumber
     state decide
     ten1 =num1
     one1 =num2
     ten2 =num3
     one2 =num4
    =retrieval>
     isa difference
     nr1 =num1
     nr2 =num3
     result =result
    < result 1
    ==>
    =goal>
     isa maxnumber
     state done
     ten-ans =num1
     one-ans =num2
    +action>
     isa outcome
     one-ans =num2
     ten-ans =num1
)


;;if the result of “tens” nr1-nr2 is a negative value, number2 is the maximum value
(p tens-value2-is-max
    =goal>
     isa maxnumber
     state decide
     ten1 =num1
     one1 =num2
     ten2 =num3
     one2 =num4
    =retrieval>
     isa difference
     nr1 =num1
     nr2 =num3
     result =result
    > result -1
    ==>
    =goal>
     isa maxnumber
     state done
     ten-ans =num3
     one-ans =num4
    +action>
     isa outcome
     one-ans =num4
     ten-ans =num3
)


(goal-focus goal)

;;--------------------------------------ADDITION---------------------------------------------------------------


;;chunks:
;;(isa addition ten1 one1 ten2 one2 ten-ans one-ans state)
;;(isa numbers nr1 nr2 result) (nr1 + nr2 = result)


;;start looking for the 'one' values (one1 and one2) you want to retrieve
(p start
    =goal>
     isa addition
     state start
     one1 =num1
     one2 =num2
    ==>
    =goal>
     isa addition
     state add-ones
    +retrieval>
     isa numbers
     nr1 =num1
     nr2 =num2
)

;;retrieve the 'one' values, if the sum of the 'one' value (result) >= 10
;;then there is a carry, we store the sum of the 'one' values in 'one-ans' and we continue to find the remainder
(p find-ones-with-a-carry
    =goal>
     isa addition
     state add-ones
     one1 =num1
     one2 =num2
    =retrieval>
     isa numbers
     nr1 =num1
     nr2 =num2
     result =result
    < result 10 ;;result >= 10
    ==>
    =goal>
     isa addition
     one-ans =result
     state find-remainder
    +retrieval>
     isa numbers
     nr1 10
     result =result
)

;;retrieve the 'one' values, if the sum of the 'one' value (result) <= 9
;;then there is no carry, the sum of the 'one' values is stored in one-ans and we can continue with looking for the 'ten' values
(p find-ones-without-a-carry
    =goal>
     isa addition
     state add-ones
     one1 =num1
     one2 =num2
    =retrieval>
     isa numbers
     nr1 =num1
     nr2 =num2
     result =result
    > result 9 ;;result <= 9
    ==>
    =goal>
     isa addition
     state no-carry
     one-ans =result
    +action>
     isa outcome
     one-ans =result
)

;;if there is a carry, we need to determine the remainder. We stored the sum of the 'one' values in 'one-ans', if we give
;;nr1 the value 10, and result the value of one-ans, retrieving nr2 will give us the value of the remainder, store this value in one-ans.
;;We now have the the value of the 'ones' of the answer and we continue to find the value of the 'tens'
(p process-carry-determine-remainder
    =goal>
     isa addition
     state find-remainder
     one-ans =result
    =retrieval>
     isa numbers
     nr1 10
     nr2 =remainder
     result =result
    ==>
    =goal>
     isa addition
     state carry
     one-ans =remainder
    +action>
     isa outcome
     one-ans =remainder
)

;;There is no carry, start looking for the 'ten' values (ten1 and ten2) you want to retrieve
(p no-carry-find-tens
    =goal>
     isa addition
     state no-carry
     ten1 =num1
     ten2 =num2
    ==>
    =goal>
     isa addition
     state add-tens-no-carry
    +retrieval>
     isa numbers
     nr1 =num1
     nr2 =num2
)

;;start looking for the 'ten' values (ten1 and ten2) you want to retrieve
(p find-tens-to-add-carry-to
    =goal>
     isa addition
     state carry
     ten1 =num1
     ten2 =num2
    ==>
    =goal>
     isa addition
     state add-carry
    +retrieval>
     isa numbers
     nr1 =num1
     nr2 =num2
)

;;retrieve the sum of the 'ten' values', store this in ten-ans. Since there is a carry that needs to be added to this sum,
;;we now know which chunk needs to be retrieved in the next production
(p add-the-carry-to-result
    =goal>
     isa addition
     state add-carry
     ten1 =num1
     ten2 =num2
    =retrieval>
     isa numbers
     nr1 =num1
     nr2 =num2
     result =result
    ==>
    =goal>
     isa addition
     state add-carry-to-tens
     ten-ans =result
    +retrieval>
     isa numbers
     nr1 1
     nr2 =result
)

;;retrieve the chunk where nr1 has value 1, and nr2 has the value of the previous stored 'ten-ans'
;;the result value will be the new ten-ans value and is the final 'tens' value. Now we have the final value for
;;both the 'ones' and the 'tens'
(p add-carry-to-tens-done
    =goal>
     isa addition
     state add-carry-to-tens
     ten-ans =num1
    =retrieval>
     isa numbers
     nr1 1
     nr2 =num1
     result =result
    ==>
    =goal>
     isa addition
     ten-ans =result
    +action>
     isa outcome
     ten-ans =result
)

;;there is no carry, retrieve the sum of the 'ten' values. Now we have the final value for
;;both the 'ones' and the 'tens'
(p no-carry-add-tens-done
    =goal>
     isa addition
     state add-tens-no-carry
     ten1 =num1
     ten2 =num2
    =retrieval>
     isa numbers
     nr1 =num1
     nr2 =num2
     result =result
    ==>
    =goal>
     isa addition
     ten-ans =result
    +action>
     isa outcome
     ten-ans =result
)

(goal-focus goal)

;;------------------------------------------SUBTRACTION-----------------------------------------------------------


;;chunks:
;;(isa subtraction ten1 one1 ten2 one2 ten-ans one-ans ans state)
;;(isa difference nr1 nr2 result) (nr2-nr1 = result)
;;(isa multipy nr1 nr2 result) (nr1 * nr2 = result)
;;(isa difference-ans nr1 nr2 result) (nr1 + nr2 = result)

;;find the 'one' values (one1 and one2) you want to retrieve
(p start-find-ones
    =goal>
    isa subtraction
    state start
    one1 =num1 
    one2 =num2
    ==>
    =goal>
    isa subtraction
    state retrieve-ones
    +retrieval>
    isa difference
    nr1 =num1
    nr2 =num2
)

;;retrieve the result of one1-one2, store the retrieved value in one-ans
(p subtract-ones
    =goal>
     isa subtraction
     state retrieve-ones
     one1 =num1
     one2 =num2
    =retrieval>
     isa difference
     nr1 =num1
     nr2 =num2
     result =result
    ==>
    =goal>
     isa subtraction
     one-ans =result
     state find-tens
)

;;find the 'ten' values (ten1 and ten2) you want to retrieve
(p process-find-tens
    =goal>
     isa subtraction
     state find-tens
     ten1 =num1
     ten2 =num2
    ==>
    =goal>
     isa subtraction
     state retrieve-tens
    +retrieval>
     isa difference
     nr1 =num1
     nr2 =num2
)

;;retrieve the result of ten1-ten2, store the retrieved value in ten-ans
;;find the chunk that multiplies the ten-ans value by 10
(p subtract-tens
    =goal>
     isa subtraction
     state retrieve-tens
     ten1 =num1
     ten2 =num2
    =retrieval>
     isa difference
     nr1 =num1
     nr2 =num2
     result =result
    ==>
    =goal>
     isa subtraction
     state multiply-tens
     ten-ans =result
    +retrieval>
     isa multiply
     nr1 10
     nr2 =result
)


;;retrieve the chunck that has the result of 10 * ten-ans
;;locate the chunk that gives the result of ten-ans + one-ans
(p multiply-ten
    =goal>
     isa subtraction
     state multiply-tens
     ten-ans =num1
     one-ans =num2
    =retrieval>
     isa multiply
     nr1 10
     nr2 =num1
     result =result
    ==>
    =goal>
     isa subtraction
     ten-ans =result
     state retrieve-ans
    +retrieval>
     isa difference-ans
     nr1 =result
     nr2 =num2
)

;;retrieve the chunk that gives the result of ten-ans + one-ans, store the result in ans
(p final-answer
    =goal>
     isa subtraction
     state retrieve-ans
     ten-ans =num1
     one-ans =num2
    =retrieval>
     isa difference-ans
     nr1 =num1
     nr2 =num2
     result =result
    ==>
    =goal>
     isa subtraction
     ans =result
    +action>
     isa outcome
     ans =result
)









