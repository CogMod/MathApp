//
//  HowToViewController.swift
//  MathApp
//
//  Created by Evert vd Weit on 23-03-16.
//  Copyright © 2016 Evert van der Weit. All rights reserved.
//

import UIKit

class HowToViewController: UIViewController {
    var currentPage = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func tapLeft() {
        switch currentPage {
        case 0:
            currentPage = 2
            howToView.image = UIImage(named: "screen1.png")
            pageDots.currentPage = 2
        case 1:
            currentPage = 0
            howToView.image = UIImage(named: "screen2.png")
            pageDots.currentPage = 0
        case 2:
            currentPage = 1
            howToView.image = UIImage(named: "screen3.png")
            pageDots.currentPage = 1
        default:
            break
        }
    }
    
    @IBAction func tapRight() {
        switch currentPage {
        case 0:
            currentPage = 1
            howToView.image = UIImage(named: "screen3.png")
            pageDots.currentPage = 1
        case 1:
            currentPage = 2
            howToView.image = UIImage(named: "screen1.png")
            pageDots.currentPage = 2
        case 2:
            currentPage = 0
            howToView.image = UIImage(named: "screen2.png")
            pageDots.currentPage = 0
        default:
            break
        }
    }
    
    @IBOutlet weak var howToView: UIImageView!
    @IBAction func swipeRight(sender: UISwipeGestureRecognizer) {
        switch currentPage {
        case 0:
            currentPage = 2
            howToView.image = UIImage(named: "screen1.png")
            pageDots.currentPage = 2
        case 1:
            currentPage = 0
            howToView.image = UIImage(named: "screen2.png")
            pageDots.currentPage = 0
        case 2:
            currentPage = 1
            howToView.image = UIImage(named: "screen3.png")
            pageDots.currentPage = 1
        default:
            break
        }
    }

    @IBAction func swipeLeft(sender: UISwipeGestureRecognizer) {
        switch currentPage {
        case 0:
            currentPage = 1
            howToView.image = UIImage(named: "screen3.png")
            pageDots.currentPage = 1
        case 1:
            currentPage = 2
            howToView.image = UIImage(named: "screen1.png")
            pageDots.currentPage = 2
        case 2:
            currentPage = 0
            howToView.image = UIImage(named: "screen2.png")
            pageDots.currentPage = 0
        default:
            break
        }
    }
    @IBOutlet weak var pageDots: UIPageControl!
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
