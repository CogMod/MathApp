//
//  OptionsViewController.swift
//  MathApp
//
//  Created by Evert vd Weit on 23-02-16.
//  Copyright © 2016 Evert van der Weit. All rights reserved.
//

import UIKit

class OptionsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        gameSlider.value = Float(OptionValues.sharedInstance.gameNumber)
        gameSliderLabel.text = "\(OptionValues.sharedInstance.gameNumber)"
        difficultySlider.value = Float(OptionValues.sharedInstance.gameDifficulty)
        difficultySliderLabel.text = OptionValues.sharedInstance.difficultyLabel
        levelSlider.value = Float(OptionValues.sharedInstance.actrLevel)
        levelSliderLabel.text = OptionValues.sharedInstance.levelLabel
        if OptionValues.sharedInstance.autoAdjust == 0 {
            autoAdjust.setOn(false, animated: false)
            autoAdjust.tag = 0
        } else {
            autoAdjust.setOn(true, animated: false)
            autoAdjust.tag = 1
//            levelSlider.enabled = false
//            levelSliderLabel.alpha = 0.75
//            levelSliderLabel.textColor = UIColor.grayColor()
//            difficultySlider.enabled = false
//            difficultySliderLabel.alpha = 0.75
//            difficultySliderLabel.textColor = UIColor.grayColor()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var gameSlider: UISlider!
    @IBOutlet weak var gameSliderLabel: UILabel!
    @IBOutlet weak var difficultySlider: UISlider!
    @IBOutlet weak var difficultySliderLabel: UILabel!
    @IBOutlet weak var levelSlider: UISlider!
    @IBOutlet weak var levelSliderLabel: UILabel!
    @IBOutlet weak var autoAdjust: UISwitch!
    
    @IBAction func autoAdjustChanged(sender: UISwitch) {
        if sender.tag == 0 {
            sender.tag = 1
            OptionValues.sharedInstance.autoAdjust = sender.tag
//            levelSlider.enabled = false
//            levelSliderLabel.alpha = 0.75
//            levelSliderLabel.textColor = UIColor.grayColor()
//            difficultySlider.enabled = false
//            difficultySliderLabel.alpha = 0.75
//            difficultySliderLabel.textColor = UIColor.grayColor()
        } else {
            sender.tag = 0
            OptionValues.sharedInstance.autoAdjust = sender.tag
//            levelSlider.enabled = true
//            levelSliderLabel.alpha = 1
//            levelSliderLabel.textColor = UIColor.whiteColor()
//            difficultySlider.enabled = true
//            difficultySliderLabel.alpha = 1
//            difficultySliderLabel.textColor = UIColor.whiteColor()
        }
    }
    @IBAction func sliderValueChanged(sender: UISlider) {
        // Different tag for each slider
        let slider = sender.tag
        switch slider {
        // Game slider
        case 0:
            let currentValue = Int(sender.value)
            gameSliderLabel.text = "\(currentValue)"
            OptionValues.sharedInstance.gameNumber = currentValue
        
            // Difficulty slider
        case 1:
            let currentValue = Int(sender.value)
            OptionValues.sharedInstance.gameDifficulty = currentValue
            switch currentValue {
            case 10...23:
                difficultySliderLabel.text = "Very easy"
                OptionValues.sharedInstance.difficultyLabel = "Very easy"
            case 24...38:
                difficultySliderLabel.text = "Easy"
                OptionValues.sharedInstance.difficultyLabel = "Easy"
            case 39...53:
                difficultySliderLabel.text = "Medium"
                OptionValues.sharedInstance.difficultyLabel = "Medium"
            case 54...68:
                difficultySliderLabel.text = "Hard"
                OptionValues.sharedInstance.difficultyLabel = "Hard"
            case 69...83:
                difficultySliderLabel.text = "Very hard"
                OptionValues.sharedInstance.difficultyLabel = "Very hard"
            case 84...99:
                difficultySliderLabel.text = "Impossible"
                OptionValues.sharedInstance.difficultyLabel = "Impossible"
            default:
                break
            }
        
            // Level slider ACT-R
        case 2:
            let currentValue = Int(sender.value)
            OptionValues.sharedInstance.actrLevel = currentValue
            OptionValues.sharedInstance.modelDiffFactor = (3.0 - (0.25 * Double(currentValue)))
            switch currentValue {
            case 1...2:
                levelSliderLabel.text = "Novice"
                OptionValues.sharedInstance.levelLabel = "Novice"
            case 3...4:
                levelSliderLabel.text = "Beginner"
                OptionValues.sharedInstance.levelLabel = "Beginner"
            case 5...6:
                levelSliderLabel.text = "Intermediate"
                OptionValues.sharedInstance.levelLabel = "Intermediate"
            case 7...8:
                levelSliderLabel.text = "Expert"
                OptionValues.sharedInstance.levelLabel = "Expert"
            case 9...10:
                levelSliderLabel.text = "Master"
                OptionValues.sharedInstance.levelLabel = "Master"
            default:
                break
            }
        default:
            break
        }
            }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


