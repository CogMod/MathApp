//
//  GameViewController.swift
//  MathApp
//
//  Created by Evert van der Weit on 22/02/16.
//  Copyright © 2016 Evert van der Weit. All rights reserved.
//

import UIKit

class GameViewController: UIViewController {

    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    @IBOutlet weak var button4: UIButton!
    @IBOutlet weak var button5: UIButton!
    @IBOutlet weak var button6: UIButton!
    @IBOutlet weak var goal: UIButton!
    @IBOutlet weak var playerScore: UILabel!
    @IBOutlet weak var modelScore: UILabel!
    @IBOutlet weak var feedback: UIImageView!
    @IBOutlet weak var feedbacktext: UILabel!
    @IBOutlet weak var actrLabel: UILabel!
    @IBOutlet weak var actrLevelIndicator: UIProgressView!
    @IBOutlet weak var tilesView: UIView!
    @IBOutlet weak var gameDiffIndicator: UIProgressView!
    @IBOutlet weak var gamesLeft: UILabel!
    
    @IBOutlet var buttonCollection: Array<UIButton>?
        var previousModelSelection = [Int]()
    
    @IBAction func backButton(sender: UIButton) {
        OptionValues.sharedInstance.running = 0
    }
    
    // Import the gamelogic module
    let gl = GameLogic()
    var modelTimer = NSTimer()
    var gameCount = 0
    var gameFinished = false
    
    // The time winning tiles are still displayed
    var winDelay = 1.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        OptionValues.sharedInstance.running = 1
        setButtons()
        modelTimer.invalidate()
        modelTimer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self,
            selector: "updateModelSelections", userInfo: nil, repeats: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // 2 Placeholder functions for the NSTimer calls upon winning
    //      Both inform the gamelogic of the winner, and restart the model update timer
    func modelWon() {
        
        gameEnded("model")
        modelTimer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self,
            selector: "updateModelSelections", userInfo: nil, repeats: true)
    }
    
    func playerWon() {


        gameEnded("player")
        modelTimer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self,
            selector: "updateModelSelections", userInfo: nil, repeats: true)
        
    }
    
    // A Function that updates the model's selection of tiles (and checks if goal reached)
    func updateModelSelections() {
        if !gameFinished {
            if (gl.modelSelectedTiles.count > 0 && gl.modelSelectedTiles != previousModelSelection) {
                print("Updating model selection")
                // Check every button
                for sender in buttonCollection! {
                    // The Button value
                    let input = NSNumberFormatter().numberFromString(sender.currentTitle!)!.integerValue
                    
                    // If the button is selected by the model,only act if button was not selected before
                    if gl.modelSelectedTiles.contains(input) {
                        switch sender.tag {
                        case 0:
                            // No one has selected the button yet, set to state 2 (only model selected)
                            sender.tag = 2
                            sender.setBackgroundImage(UIImage(named: "buttonGrey75.png"), forState: .Normal)
                        case 1:
                            // Uses also selected this button set to state 3 (Both selected)
                            sender.tag = 3
                            sender.setBackgroundImage(UIImage(named: "buttonRedGrey75.png"), forState: .Normal)
                        default: break
                        }
                    } else {
                        // If the button is not selected by the model, only act if the model had selected it before
                        switch sender.tag {
                        case 2:
                            // Only model has selected the button, set to state 0 (no-one selected)
                            sender.tag = 0
                            sender.setBackgroundImage(UIImage(named: "buttonNormal75.png"), forState: .Normal)
                        case 3:
                            // Both have selected the tile, set to state 1 (only player selected)
                            sender.tag = 1
                            sender.setBackgroundImage(UIImage(named: "buttonRed75.png"), forState: .Normal)
                        default: break
                        }
                    }
                }
                previousModelSelection = gl.modelSelectedTiles
                // Check if the model has won the game
                if gl.modelSelectedTiles.reduce(0, combine: +) == gl.goal[0] {
                    // Disable the buttons
                    tilesView.userInteractionEnabled = false
                    gameFinished = true
                    
                    feedbacktext.text = "Too slow"
                    feedbacktext.backgroundColor = UIColor(red: 160/255, green: 160/255, blue: 160/255, alpha: 1.0)
                    feedbacktext.hidden = false
                    feedback.hidden = false

                    modelTimer.invalidate()
                    modelTimer = NSTimer.scheduledTimerWithTimeInterval(winDelay, target: self,
                        selector: "modelWon", userInfo: nil, repeats: false)
                }
            }
        }
    }
    
    @IBAction func buttonPressed(sender: UIButton) {
        if !gameFinished {
            // Adjust the correct image for the selected button
            // Use tags of buttons to identify current image
            switch sender.tag {
            case 0:
                sender.tag = 1
                sender.setBackgroundImage(UIImage(named: "buttonRed75.png"), forState: UIControlState.Normal)
                // Button selected by user, so deselect
            case 1:
                sender.tag = 0
                sender.setBackgroundImage(UIImage(named: "buttonNormal75.png"), forState: UIControlState.Normal)
                // Button selected by ACT-R, so add user color
            case 2:
                sender.tag = 3
                sender.setBackgroundImage(UIImage(named: "buttonRedGrey75.png"), forState: UIControlState.Normal)
                // Button selected by both, remove player color
            case 3:
                sender.tag = 2
                sender.setBackgroundImage(UIImage(named: "buttonGrey75.png"), forState: UIControlState.Normal)
            default:
                break
            }
            // Feed the click to the controller
            let input = NSNumberFormatter().numberFromString(sender.currentTitle!)!.integerValue
            gl.clickTile(input)
            
            // Check if goal is reached
            if gl.selectedTiles.reduce(0, combine: +) == gl.goal[0] {
                gameFinished = true
                tilesView.userInteractionEnabled = false
                feedbacktext.backgroundColor = UIColor(red: 119/255, green: 15/255, blue: 0.0, alpha: 1.0)
                feedbacktext.text = "Correct"
                feedbacktext.hidden = false
                feedback.hidden = false
                modelTimer.invalidate()
                modelTimer = NSTimer.scheduledTimerWithTimeInterval(winDelay, target: self,
                    selector: "playerWon", userInfo: nil, repeats: false)
            }
        }
    }
    
    func gameEnded(winner:String) {
        gameCount++
        switch winner {
        case "player":
            // Update the score
            let oldNumber = NSNumberFormatter().numberFromString(playerScore.text!)!.integerValue
            playerScore.text = String(oldNumber+1)
        case "model":
            // Update the score
             let oldNumber = NSNumberFormatter().numberFromString(modelScore.text!)!.integerValue
            modelScore.text = String(oldNumber+1)
        default: break
        }
        
        feedback.hidden = true
        feedbacktext.hidden = true
        tilesView.userInteractionEnabled = true
        
        switch OptionValues.sharedInstance.modelDiffFactor {
        case 0.50...0.74:
            OptionValues.sharedInstance.actrLevel = 10
            OptionValues.sharedInstance.levelLabel = "Master"
        case 0.75...0.99:
            OptionValues.sharedInstance.actrLevel = 9
            OptionValues.sharedInstance.levelLabel = "Master"
        case 1.00...1.24:
            OptionValues.sharedInstance.actrLevel = 8
            OptionValues.sharedInstance.levelLabel = "Expert"
        case 1.25...1.49:
            OptionValues.sharedInstance.actrLevel = 7
            OptionValues.sharedInstance.levelLabel = "Expert"
        case 1.50...1.74:
            OptionValues.sharedInstance.actrLevel = 6
            OptionValues.sharedInstance.levelLabel = "Intermediate"
        case 1.75...1.99:
            OptionValues.sharedInstance.actrLevel = 5
            OptionValues.sharedInstance.levelLabel = "Intermediate"
        case 2.00...2.24:
            OptionValues.sharedInstance.actrLevel = 4
            OptionValues.sharedInstance.levelLabel = "Beginner"
        case 2.25...2.49:
            OptionValues.sharedInstance.actrLevel = 3
            OptionValues.sharedInstance.levelLabel = "Beginner"
        case 2.50...2.74:
            OptionValues.sharedInstance.actrLevel = 2
            OptionValues.sharedInstance.levelLabel = "Novice"
        case 2.75...3.00:
            OptionValues.sharedInstance.actrLevel = 1
            OptionValues.sharedInstance.levelLabel = "Novice"
        default:
            break
        }
        
        switch OptionValues.sharedInstance.gameDifficulty {
        case 10...23:
            OptionValues.sharedInstance.difficultyLabel = "Very easy"
        case 24...38:
            OptionValues.sharedInstance.difficultyLabel = "Easy"
        case 39...53:
            OptionValues.sharedInstance.difficultyLabel = "Medium"
        case 54...68:
            OptionValues.sharedInstance.difficultyLabel = "Hard"
        case 69...83:
            OptionValues.sharedInstance.difficultyLabel = "Very hard"
        case 84...99:
            OptionValues.sharedInstance.difficultyLabel = "Impossible"
        default:
            break
        }

        
        // Check if we reached the max number of games specified
        if gameCount >= OptionValues.sharedInstance.gameNumber {
            let player = NSNumberFormatter().numberFromString(playerScore.text!)!.integerValue
            let model = NSNumberFormatter().numberFromString(modelScore.text!)!.integerValue
            // Present the end game screen
            feedback.alpha = 0.8
            if player > model {
                feedbacktext.text = "You won! Good job!"
                feedbacktext.backgroundColor = UIColor(red: 119/255, green: 15/255, blue: 0.0, alpha: 1.0)
            } else if model > player {
                feedbacktext.text = "You lost, keep trying!"
                feedbacktext.backgroundColor = UIColor(red: 160/255, green: 160/255, blue: 160/255, alpha: 1.0)
            } else {
                feedbacktext.text = "Draw! Keep it up!"
                feedbacktext.backgroundColor = UIColor(red: 119/255, green: 15/255, blue: 0.0, alpha: 1.0)
            }
            
            feedbacktext.hidden = false
            feedback.hidden = false
            modelTimer = NSTimer.scheduledTimerWithTimeInterval(2.5, target: self,
                selector: "backToMain", userInfo: nil, repeats: false)
            
        } else {
            // Reset the buttons
            if OptionValues.sharedInstance.autoAdjust == 1 {
                gl.addWinner(winner)
                setButtons()
            } else {
                gl.reset()
                setButtons()
            }
        }
    }

    func backToMain () {
        performSegueWithIdentifier("backToMain", sender: nil)
        OptionValues.sharedInstance.running = 0
    }
    
    func setButtons() {
        
        // Get the generated gametiles and shuffle them
        var gameTiles = gl.goodTiles + gl.badTiles
        gameTiles.shuffleInPlace()
        
        // Set the goal and the gametiles
        goal.setTitle(String(gl.goal[0]),forState: .Normal)
        for (index, button) in (buttonCollection?.enumerate())! {
            button.setTitle(String(gameTiles[index]), forState: .Normal)
            button.setBackgroundImage(UIImage(named: "buttonNormal75.png"), forState: UIControlState.Normal)
            button.tag = 0
        }
        
        // Set how many games left
        gamesLeft.text = "\(OptionValues.sharedInstance.gameNumber - gameCount)"
        
        // Set the progress and gamediff bar
        let progressActr = Float(0.1 * Double(OptionValues.sharedInstance.actrLevel))
        let progressGameDiff = Float(0.01 + 0.01 * Double(OptionValues.sharedInstance.gameDifficulty))
        print("Progress ACT-R: \(progressActr)")
        print("Progress gameDiff: \(progressGameDiff)")
        actrLevelIndicator.progressTintColor = progressToColor(progressActr)
        actrLevelIndicator.progress = progressActr
        gameDiffIndicator.trackTintColor = progressToColor(progressGameDiff)
        gameDiffIndicator.progress = (1 - progressGameDiff)
        gameFinished = false

    }
    
    func progressToColor (value:Float) -> UIColor {
        switch value {
        case 0.0..<0.2:
            return UIColor.greenColor()
        case 0.2..<0.4:
            return UIColor.greenColor()
        case 0.4..<0.6:
            return UIColor.yellowColor()
        case 0.6..<0.8:
            return UIColor.orangeColor()
        case 0.8...1:
            return UIColor.redColor()
        default: return UIColor.blackColor()
        }
    }
}
