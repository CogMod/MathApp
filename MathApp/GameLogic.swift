//
//  GameLogic.swift
//  MathApp
//
//  Created by Evert vd Weit on 03-03-16.
//  Copyright © 2016 Evert van der Weit. All rights reserved.
//

import Foundation

// Add the shuffle function to arrays
extension MutableCollectionType where Index == Int {
    /// Shuffle the elements of `self` in-place.
    mutating func shuffleInPlace() {
        // empty and single-element collections don't shuffle
        if count < 2 { return }
        
        for i in 0..<count - 1 {
            let j = Int(arc4random_uniform(UInt32(count - i))) + i
            guard i != j else { continue }
            swap(&self[i], &self[j])
        }
    }
}

class GameLogic {
		
    // GAME VARIABLES
    var maxGoal = OptionValues.sharedInstance.gameDifficulty
    var solutionSize: Int
    var goal = [Int]()
    var goodTiles = [Int]()
    var badTiles = [Int]()
    var selectedTiles = [Int]()
    var modelSelectedTiles = [Int]()
    var modelBlacklistTiles = [Int]()
    var lastFiveWinners = [String]()
    
    // MODEL VARIABLES
    var timer = NSTimer()
    var counter = 0
    
    // Finding the max value
    var modelState = "findMax"
    var arrayOfValues = [Int]()
    var foundMax = 0
    var findMaxStep = 0
    var nrSteps = 0
    
    // Subtraction
    var difference = 0.0
    
    // The internal sum
    var internalSum = 0
    
    // The delay of the model in ms
    var modelDiffFactor = (3.0 - (0.25 * Double(OptionValues.sharedInstance.actrLevel)))
    
    // The actual ACT-R Brain
    var brain:MathBrain
    
    init() {
//        brain = MathBrain(maxGoal: maxGoal)
        brain = OptionValues.sharedInstance.actrBrain
        solutionSize = 2 + Int(arc4random_uniform(3))
        generateNumbers(maxGoal,solutionSize: solutionSize)
    }
    
    func addWinner (winner: String) {
        // Add the winner, and make sure only the last five are remembered
        lastFiveWinners.append(winner)
        while (lastFiveWinners.count > 5) {
            lastFiveWinners.removeFirst()
        }
        if lastFiveWinners.count == 5 {
            var modelWins = 0
            for winner in lastFiveWinners {
                if winner == "model" { modelWins++ }
            }
            
            // Adjust the difficulty (if five games are played
            if modelWins >= 3 {
                // Model too good, so increase timefactor and decrease the maxgoal
                modelDiffFactor = min(3.0, modelDiffFactor*1.05)
                OptionValues.sharedInstance.modelDiffFactor = modelDiffFactor
                if OptionValues.sharedInstance.gameDifficulty < 20 {
                    maxGoal = max(10, maxGoal--)
                } else {
                    maxGoal = min(99, Int(floor(Double(maxGoal)*0.95)))
                }
                OptionValues.sharedInstance.gameDifficulty = maxGoal
            } else {
                // Model not fast enough, so decrease timefactor and increase the maxgoal
                modelDiffFactor = max(0.5, modelDiffFactor*0.95)
                OptionValues.sharedInstance.modelDiffFactor = modelDiffFactor
                if OptionValues.sharedInstance.gameDifficulty < 20 {
                    maxGoal++
                } else {
                    maxGoal = min(99, Int(floor(Double(maxGoal)*1.05)))
                }
                OptionValues.sharedInstance.gameDifficulty = maxGoal
            }
            
            print("")
            print("Last five winners: \(lastFiveWinners)")
            print("\t The new difficulty factor is: \(modelDiffFactor)")
            print("\t The new maxGoal is: \(maxGoal)")
            print("")
        } else {
            print("Cannot adjust difficulty, need \(5-lastFiveWinners.count) more game(s)")
        }
        // Now reset the game
        reset()
    }
    
    func reset() {
        self.selectedTiles = [Int]()
        resetModel()
        solutionSize = 2 + Int(arc4random_uniform(3))
        generateNumbers(maxGoal,solutionSize: solutionSize)
        print(" ")
        print(" ")
        print(" ")
        print(" ")
        print(" ")
        print(" ")
    }
    
    func resetModel() {
        modelSelectedTiles  = [Int]()
        modelBlacklistTiles = [Int]()
        modelState = "findMax"
        arrayOfValues = [Int]()
        foundMax = 0
        findMaxStep = 0
        internalSum = -1
    }
    
    func getPossibleValues () -> [Int] {
        // All the possible values
        var possible = goodTiles + badTiles
        
        // Remove all the already found values
        for value in modelSelectedTiles {
            possible.removeAtIndex(possible.indexOf(value)!)
        }
        // Remove the blacklisted values
        for value in modelBlacklistTiles {
            possible.removeAtIndex(possible.indexOf(value)!)
        }
        return possible
    }
    
    dynamic func runModel() {
        timer.invalidate()
        var timeToPass: Double
        
        switch modelState {
        case "findMax":
            print("findMaxStep, step: \(findMaxStep)")
            timeToPass = findMaxValue()
        case "subtract":
            print("Subtracting")
            timeToPass = modelSubtract()
        case "checkDiff":
            print("Checking for difference")
            timeToPass = modelCheckForDiff()
        case "addingToSum":
            print("adding the selected value to our internal sum")
            timeToPass = addToSum()
            
        default:
            print("No valid state set")
            timeToPass = 1.0
        }
        // Set the new timer
        if OptionValues.sharedInstance.running == 1 {
            timer = NSTimer.scheduledTimerWithTimeInterval(
                timeToPass*modelDiffFactor,
                target: self,
                selector: "runModel",
                userInfo: nil,
                repeats: false
            )
        }
    }
    
    func addToSum () -> Double {
        var timePassed = 0.0
        var answer = 0.0
        // First time adding to the sum
        if internalSum < 0 {
            internalSum = foundMax
        } else {
            // Add the found max value to our internal sum
            (timePassed, answer) = brain.addition(internalSum, val2: foundMax)
            // Check if we don't succeed the goal
            internalSum = Int(answer)
        }
        modelState = "subtract"
        return timePassed
    }
    
    func modelCheckForDiff () -> Double {
        // Get the remainingValues
        arrayOfValues = getPossibleValues()
        
        if arrayOfValues.contains(Int(difference)) {
            print("Button containing the difference found")
            modelSelectedTiles.append(Int(difference))
        } else {
            print("Button containing the difference not found")
            // Start looking for the next value
            modelState = "findMax"
        }
        return (0.1)
    }
    
    func modelSubtract() -> Double {
        var timePassed: Double
        // Subtract the internal sum (val2) from the goal value (val1)
        (timePassed, difference) = brain.subtract(goal[0], val2: internalSum)
        modelState = "checkDiff"
        return timePassed
    }
    
    
    // Find the maximum value in the possible tiles
    func findMaxValue () -> Double {
        var timePassed = 0.0
        
        // Get the number of available tiles
        nrSteps = getPossibleValues().count - 1
        
        // If the blacklist is filled with all remaining tiles
        //  Clear the blacklist
        if nrSteps <= 0 {
            print("No steps available")
            // Remove unnecesary values from the blacklist
            var newBlacklist = [modelBlacklistTiles.last!]
            for item in modelBlacklistTiles {
                if item > newBlacklist[0] {
                    newBlacklist.append(item)
                }
            }
            
            // Reset findMaxValue parameters, en re-enter
            modelBlacklistTiles = newBlacklist
            findMaxStep = 0
            modelState = "findMax"
            nrSteps = getPossibleValues().count - 1
            print("New number of steps: \(nrSteps)")
            return(0)
        }
        
        outerSwitch: switch findMaxStep {
        
        // From second to to last iteration
        case 1...nrSteps:
            // If there is another possible value
            if arrayOfValues.count != 0 {
                let value = arrayOfValues.removeFirst()
                
                // Only compare if the value is possible (below goal)
                if (value + internalSum <= goal[0]) {
                    
                    var maxValue = 0.0

                    // Compare value to the previously remembered foundMax
                    // Largest value is named first
                    (timePassed, maxValue) = brain.getMaxValue(
                              max(value, foundMax),
                        val2: min(value, foundMax)
                    )
                    
                    foundMax = Int(maxValue)
                }
            }
            // Final iteration
            if findMaxStep == nrSteps {
                print("Found The final max value \(foundMax)")
                // Select the max value
                modelSelectedTiles.append(foundMax)
                
                // Reset the foundmax variables
                arrayOfValues = [Int]()
                findMaxStep = 0
                
                // Add the found max value to the internal sum
                modelState = "addingToSum"
                return timePassed
            }
            
        // The first iteration
        case 0:
            // Get all the possible values
            arrayOfValues = getPossibleValues()
            print("Possible values at start of finding max: \(arrayOfValues)")
            print("Nr of steps: \(arrayOfValues.count-1)")
            
            // Shuffle the values
            arrayOfValues.shuffleInPlace()
            
            // Take a first random value from the list and remember
            repeat {
                // If no good value can be found
                if (arrayOfValues.count) == 0 {
                    print("Can't find correct value, put the last found value on a blacklist")
                    // Remove the last found value and put it on a blacklist
                    let lastFoundValue = modelSelectedTiles.removeLast()
                    internalSum -= lastFoundValue
                    modelBlacklistTiles.append(lastFoundValue)
                    print("Added \(lastFoundValue) to the blacklist")
                    
                    // Reset the findMax function variables
                    findMaxStep = 0
                    modelState = "findMax"
                    // Break the switch
                    return(0)
                }
                // Save the first found value as max
                foundMax = arrayOfValues.removeFirst()
                
            } while (foundMax + internalSum > goal[0])
            
        default: break
        }
        
        findMaxStep++
        return timePassed
    }
    
        
    func generateNumbers(maxGoal: Int, solutionSize: Int) {
        // The goal is generated in a range of <factorial solutionSize, maxGoal>
//        let minimum = 10
        let diff = maxGoal - Int(arc4random_uniform(UInt32(Int(0.25 * Float(maxGoal)))))
        print(diff)
//        self.goal = [(min + Int(arc4random_uniform(UInt32(diff))))]
        self.goal = [diff]
        
        self.goodTiles = [Int](count: solutionSize, repeatedValue: 0)
        var sum = 0
        var last = 0
        while (last <= 0 && goodTiles.minElement() <= 0) || goodTiles.contains(last) {
            sum = 0
            for var id = 0; id < solutionSize-1; ++id {
                var number = 1+Int(arc4random_uniform(UInt32(goal[0]-1)))
                while goodTiles.contains(number) {
                    number = 1+Int(arc4random_uniform(UInt32(goal[0]-1)))
                }
                goodTiles[id] = number
                sum += number
            }
            last = goal[0] - sum
        }
        self.goodTiles[solutionSize-1] = last
        
        self.badTiles = [Int](count: 6 - solutionSize, repeatedValue: 0)
        for var id = 0; id < 6-solutionSize; ++id {
            // The remaining numbers can't be already in the 
            var number = Int(arc4random_uniform(UInt32(goal[0])))
                while goodTiles.contains(number) || badTiles.contains(number) {
                    number = Int(arc4random_uniform(UInt32(goal[0])))
            }
            badTiles[id] = number
        }
        
        // The game is ready, start the model
        timer.invalidate()
        timer = NSTimer.scheduledTimerWithTimeInterval(0.2, target: self,
            selector: "runModel", userInfo: nil, repeats: false)
    }
    
    // Update the user-clicked tiles
    func clickTile(number: Int) {
        if self.selectedTiles.contains(number) {
            self.selectedTiles.removeAtIndex(self.selectedTiles.indexOf(number)!)
        } else {
            self.selectedTiles.append(number)
        }
        print("selected tiles: \(self.selectedTiles)")
    }
}